<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
	</head>
	<body>
	<div class="errorMessageArea">${errorMessages}</div>
	<div class="header">
		<a href="home">ホームへ</a>
	</div>
	<div class=" form salesFileReadForm">
		<label>売り上げファイル読み込み</label>
		<form:form modelAttribute="readFileForm" name="myform">
		    <input name="myfile" type="file"  />
		    <form:input type="hidden" path="readFileText" />
		    <input type="submit" name="sales" value="参照">
		    <textarea rows="10" cols="10" name="textarea"></textarea>
	 	</form:form>
 	</div>

 	<div class="form branchFileReadForm">
	 	<label>支店定義ファイル読み込み</label>
	 	<form:form modelAttribute="branchForm" name="branchForm">
		    <input name="branch_file" type="file"  />
		    <form:input type="hidden" path="brachList" />
		    <input type="submit" name="branch" value="参照">
		    <textarea rows="10" cols="10" name="textarea"></textarea>
	 	</form:form>
 	</div>

 	<a href="home">ホームへ</a>



	<script type="text/javascript">
		 //Form要素を取得する
	    var form = document.forms.myform;

	    //ファイルが読み込まれた時の処理
	    form.myfile.addEventListener('change', function(e) {
	    	var files = form.myfile.files;
	    	var reader = new FileReader();
    		var readTextList = {};
	    	for(var i = 0; i < files.length; i++){
	    		  reader.readAsText(files[i]);
	    	}

	    	reader.onload = function(en){
	    		var csvTextList = getCsv(reader.result);
	    		document.myform.readFileText.value = csvTextList;
	    		document.myform.textarea.value = reader.result;
	    	}



	    },false);

	    var branchForm = document.forms.branchForm;
	    branchForm.branch_file.addEventListener('change', function(e) {
	    	var files = branchForm.branch_file.files;
	    	var reader = new FileReader();
    		var readTextList = {};
	    	for(var i = 0; i < files.length; i++){
	    		  reader.readAsText(files[i]);
	    	}

	    	reader.onload = function(en){
	    		var csvTextList = getCsv(reader.result);
	    		document.branchForm.brachList.value = csvTextList;
	    		document.branchForm.textarea.value = reader.result;
	    	}



	    },false);

	    //CSVファイルの中身を毎行カンマ区切りでリストにつめていく
	    function getCsv(text){
	   		 var textRowList = text.split('\n');
	   		 var textSplitList = [];
	   		 for(var j = 0; j < textRowList.length; j++){
	   			 if(textRowList[j] == '')break;
	   			textSplitList[j] = textRowList[j].split(",");
	   		 }
	   		 return textRowList;
   		}


	</script>
</body>

</html>