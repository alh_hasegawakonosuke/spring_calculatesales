package jp.co.calculate.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.calculate.dto.UserDto;
import jp.co.calculate.form.RegistForm;
import jp.co.calculate.mapper.UserMapper;

@Service
public class UserSettingService {

	@Autowired
	private UserMapper userMapper;

	public int registUser(RegistForm form) {
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(form, dto);
		int count = userMapper.insertUser(dto);
		return count;
	}
}
