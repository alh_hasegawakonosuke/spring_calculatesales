package jp.co.calculate.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.calculate.dto.UserDto;
import jp.co.calculate.entity.User;
import jp.co.calculate.form.LoginUserForm;
import jp.co.calculate.mapper.LoginMapper;

@Service
public class LoginService {

	@Autowired
	LoginMapper loginMapper;

	@Transactional
	public UserDto getLoginUser(LoginUserForm form) {
		UserDto user = new UserDto();
		User entity = new User();
		try {
			entity = loginMapper.getLoginUser(form);
			BeanUtils.copyProperties(entity, user);
		} catch(IllegalArgumentException e){
			user = null;
		}
		return user;
	}

}
