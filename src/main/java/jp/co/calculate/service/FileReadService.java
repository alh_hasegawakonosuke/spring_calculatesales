package jp.co.calculate.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import jp.co.calculate.form.BranchForm;
import jp.co.calculate.form.ReadFileTextForm;
import jp.co.calculate.form.SalesForm;
import jp.co.calculate.form.UploadBranchFileForm;
import jp.co.calculate.mapper.FileReadMapper;

@Service
public class FileReadService {

	@Autowired
	FileReadMapper fileReadMapper;

//	public void readSCV(UploadFileForm form) {
//		try {
//			MultipartFile multipartFile = form.getMultipartFile();
//			InputStream stream = multipartFile.getInputStream();
////			File file = form.getFile();
////			FileReader fr = new FileReader(file);
//
//			//FileReader fr = new FileReader(file);
//			Reader reader = new InputStreamReader(stream);
//
//
//			BufferedReader br = new BufferedReader(reader);
//
//			String line;
//			SalesForm sales = new SalesForm();
//			int count = 0;
//			while((line = br.readLine()) != null){
//				if(count != 0) {
//					sales.setBranch_code(line.split(",", -1)[0]);
//					sales.setSales(Integer.parseInt(line.split(",", -1)[1]));
//					fileReadMapper.insertDate(sales);
//					count ++;
//				}
//				count ++;
//			}
//		}catch(IOException e) {
//
//		}
//	}

	public void readBranchList(UploadBranchFileForm textList) throws DuplicateKeyException{
		List<String> readTextList = new ArrayList<String>();
		readTextList = textList.getBrachList();
		BranchForm branch = new BranchForm();
			for(int i = 0; i < readTextList.size(); i++) {
				if(i != 0 && i != 1) {
					if(i % 2 == 0) {
						branch.setBranchCode(readTextList.get(i));
					} else {
						branch.setBranchName(readTextList.get(i));
						fileReadMapper.insertBranchDate(branch);
					}
				}
			}
	}

	public void readTextList(ReadFileTextForm textList) throws DuplicateKeyException{
		List<String> readTextList = new ArrayList<String>();
		readTextList = textList.getReadFileText();
		SalesForm sales = new SalesForm();
			for(int i = 0; i < readTextList.size(); i++) {
				if(i != 0 && i != 1) {
					if(i % 2 == 0) {
						sales.setBranch_code(readTextList.get(i));
					} else {
						sales.setSales(Integer.parseInt(readTextList.get(i)));
						fileReadMapper.insertDate(sales);
					}
				}

			}

	}

}
