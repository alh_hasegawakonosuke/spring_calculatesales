package jp.co.calculate.mapper;

import jp.co.calculate.form.BranchForm;
import jp.co.calculate.form.SalesForm;

public interface FileReadMapper {
	void insertDate(SalesForm form);
	void insertBranchDate(BranchForm form);
}
