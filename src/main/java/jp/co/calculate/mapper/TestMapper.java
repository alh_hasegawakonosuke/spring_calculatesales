package jp.co.calculate.mapper;

import jp.co.calculate.entity.Test;

public interface TestMapper {
    Test getTest(int id);
}