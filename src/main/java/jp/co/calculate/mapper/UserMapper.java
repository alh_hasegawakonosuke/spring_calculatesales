package jp.co.calculate.mapper;

import jp.co.calculate.dto.UserDto;

public interface UserMapper {
	int insertUser(UserDto dto);
}
