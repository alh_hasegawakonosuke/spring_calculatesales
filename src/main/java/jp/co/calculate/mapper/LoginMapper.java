package jp.co.calculate.mapper;

import jp.co.calculate.entity.User;
import jp.co.calculate.form.LoginUserForm;

public interface LoginMapper {
	User getLoginUser(LoginUserForm form);

}
