package jp.co.calculate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@SessionAttributes("user")
public class HomeController {


	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String getPage(Model model) {

		return "home";
	}

	@RequestMapping(value="/home", params="logout", method = RequestMethod.POST)
	public String logout(Model model, SessionStatus sessionStuts) {
		sessionStuts.setComplete();
		return "redirect:/login";
	}




}


