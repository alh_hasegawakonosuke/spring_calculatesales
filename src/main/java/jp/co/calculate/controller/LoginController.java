package jp.co.calculate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.calculate.dto.UserDto;
import jp.co.calculate.form.LoginUserForm;
import jp.co.calculate.service.LoginService;

@SessionAttributes("user")
@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getPage(Model model) {
		LoginUserForm form = new LoginUserForm();
		model.addAttribute("loginUserForm", form);
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String enterHome(Model model, @ModelAttribute LoginUserForm form, BindingResult result) {
		UserDto user = loginService.getLoginUser(form);
		if(user != null) {
			model.addAttribute("user", user);
			return "redirect:/home";
		}
		else {
			model.addAttribute("loginUserForm", form);
			model.addAttribute("errorMessage", "ユーザー名もしくはユーザーIDが異なります");

			return "login";
		}
	}
}
