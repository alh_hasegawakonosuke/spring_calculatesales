package jp.co.calculate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.calculate.form.ReadFileTextForm;
import jp.co.calculate.form.UploadBranchFileForm;
import jp.co.calculate.service.FileReadService;

@Controller
public class FileUploadController {

	@Autowired
	FileReadService fileReadService;

	@RequestMapping(value="/upload", method=RequestMethod.GET)
		public String getPage(Model model) {
		ReadFileTextForm form = new ReadFileTextForm();
		UploadBranchFileForm branchForm = new UploadBranchFileForm();
		model.addAttribute("readFileForm", form);
		model.addAttribute("branchForm", branchForm);
		return "fileUpload";
	}

	@RequestMapping(value = "/upload", params="sales", method = RequestMethod.POST)
	public String upload(Model model, @ModelAttribute("readFileForm") ReadFileTextForm form, BindingResult result) {
		try {
			fileReadService.readTextList(form);
			return "redirect:home";
		} catch (DuplicateKeyException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			ReadFileTextForm fileForm = new ReadFileTextForm();
			UploadBranchFileForm branchForm = new UploadBranchFileForm();
			model.addAttribute("readFileForm", fileForm);
			model.addAttribute("branchForm", branchForm);
			model.addAttribute("errorMessages", "このファイルは既に登録されています");
			return "fileUpload";
		}
	}

	@RequestMapping(value = "/upload", params = "branch", method = RequestMethod.POST)
	public String upload(Model model, @ModelAttribute("branchForm") UploadBranchFileForm form, BindingResult result) {
		try {
			fileReadService.readBranchList(form);
			return "redirect:home";
		} catch (DuplicateKeyException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			ReadFileTextForm fileForm = new ReadFileTextForm();
			UploadBranchFileForm branchForm = new UploadBranchFileForm();
			model.addAttribute("readFileForm", fileForm);
			model.addAttribute("branchForm", branchForm);
			model.addAttribute("errorMessages", "このファイルは既に登録されています");
			return "fileUpload";
		}
	}

}
