package jp.co.calculate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ReactController {

	@RequestMapping(value="/react", method = RequestMethod.GET)
	public String getPage(Model model) {
		return "react";
	}
}
