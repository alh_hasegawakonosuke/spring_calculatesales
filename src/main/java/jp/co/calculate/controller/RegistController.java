package jp.co.calculate.controller;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.calculate.form.RegistForm;
import jp.co.calculate.service.UserSettingService;

@Controller
public class RegistController {

	@Autowired
	UserSettingService userSettingService;

	@RequestMapping(value = "/regist", method = RequestMethod.GET)
	public String getRegistForm(Model model) {
		RegistForm form = new RegistForm();
		model.addAttribute("RegistForm", form);
		return "regist";
	}

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public String registUser(Model model, @Valid @ModelAttribute RegistForm form, BindingResult result){
		if(result.hasErrors()) {
			model.addAttribute("message", "以下のエラーを解消してください");
			model.addAttribute("RegistForm", form);
			return "regist";
		} else {
			int count = userSettingService.registUser(form);
			Logger.getLogger(RegistController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		}

		return "redirect:/regist";

	}

}
