package jp.co.calculate.form;

import org.springframework.web.multipart.MultipartFile;

public class UploadFileForm {

	private MultipartFile multipartFile;

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}


}
