package jp.co.calculate.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class RegistForm {
	@NotEmpty(message = "ユーザー名は必須です")
	private String name;

	@NotNull(message = "ユーザーIDは必須です")
	private Integer login_id;

	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name = name;
	}
	public Integer getlogin_id() {
		return login_id;
	}
	public void setlogin_id(Integer login_id) {
		this.login_id = login_id;
	}
}
