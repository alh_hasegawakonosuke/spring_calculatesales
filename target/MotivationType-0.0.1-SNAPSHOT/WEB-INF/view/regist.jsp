<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
			integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
		<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
	</head>

	<body>
		<div  class="header">
			<a href="home">ホーム画面へ</a>
		</div>
		<div class="text-center">
			<h1>ユーザー登録画面【仮】</h1>
			<h2>${user.name}</h2>
		</div>


			<form:form modelAttribute = "RegistForm">
				<div>
					<form:errors path="*"></form:errors>
				</div>
				<div class="text-center">
					<form:label path="name" class="col-md-1">ユーザー名</form:label>
					<form:input path="name" class="col-md-2"/>
				</div>
				<div class="text-center">
					<form:label path="login_id" class="col-md-1">ユーザーID</form:label>
					<form:input path="login_id" class="col-md-2"/>
				</div>
				<div class="text-center">
					<input  type="submit" value="登録">
				</div>
			</form:form>

	</body>
</html>