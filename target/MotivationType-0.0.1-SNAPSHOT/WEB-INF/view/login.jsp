<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ホーム画面</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
			integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	</head>
	<body>

		<div class="text-center error">${errorMessage}</div>

		<h1 class="text-center">ログイン画面【仮】</h1>
			<form:form modelAttribute = "loginUserForm">
				<div class="text-center">
					<form:label path="name" class="col-md-1">ユーザー名</form:label>
					<form:input path="name" class="col-md-2" />
				</div>
				<div class="text-center">
					<form:label path="login_id" class="col-md-1">ユーザーID</form:label>
					<form:input path="login_id" class="col-md-2"/>
				</div>

				<input class="offset-md-7" type="submit" value="ENTER">
			</form:form>
	</body>
</html>