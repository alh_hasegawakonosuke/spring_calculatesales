<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>ホーム画面</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
			integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

	</head>

	<body>
		<script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
		<script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
		<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>

		<script src="<c:url value="resources/js/reactTest.js" />" type="text/babel"></script>

		<h1 class="text-center">ホーム画面【仮】</h1>
		<h2 class="text-center">${user.name}</h2>
		<div class="form text-center">
			<form:form modelAttribute = "logoutForm">
				<input type="submit" name="logout" value="ログアウト">
			</form:form>
			<a href="regist">登録画面</a>



		</div>

	</body>
</html>